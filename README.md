# README #


### What is this repository for? ###
This plugin will allow to produce graph data to redis and import it very very quick into neo4j. The plugin has methods to stop, start, and status the importing process via REST.

### How do I get set up? ###
* Build the plugin via maven
* put to plugin folder of the neo4j community server
* issue "curl -X POST http://localhost:7474/db/data/ext/plugin/graphdb/start_importer_daemon"
* wait for some time - the timeline will be built before first use
* start importing entities and relationships to redis. To learn how to, have a look at how the timeline is built

Questions? stefan.rapp@avellinus.com