package com.avellinus.neo4jRedisPlugin.redis.models;

import java.util.HashMap;
import java.util.Map;

import static java.lang.String.format;

/**
 * An Object representing a link between two
 * Entities - usually a cite
 */
public class RedisLink extends RedisObject {
    private Long source_id;
    private String target_type;
    private String targetQuery;
    private Map<String, Object> linkparams = new HashMap<String, Object>();
    public RedisLink() {
    }

    /**
     * Source id of the Link
     * @return Long
     */
    public Long getSource_id() {
        return source_id;
    }

    /**
     * Set source id of the object
     * @param source_id Long
     */
    public void setSource_id(Long source_id) {
        this.source_id = source_id;
    }

    /**
     * the target type (Content type) of
     * the object
     *
     * @return String CT
     */
    public String getTarget_type() {
        return target_type;
    }

    /**
     * the type (CT) of the target is set here
     * @param target_type String
     */
    public void setTarget_type(String target_type) {
        this.target_type = target_type;
    }

    /**
     * A lucene Query for link validation
     * @return String query
     */
    public String getTargetQuery() {
        return targetQuery;
    }

    /**
     * Setting the lucene query as a string here
     *
     * @param targetQuery String
     */
    public void setTargetQuery(String targetQuery) {
        this.targetQuery = targetQuery;
    }

    /**
     * Linkparams are used to determine the target in validation
     * @return Map of String to Objects
     */
    public Map<String, Object> getLinkparams() {
        return linkparams;
    }

    /**
     * Set the linkparams
     * @param linkparams Map of String Object
     */
    public void setLinkparams(Map<String, Object> linkparams) {
        this.linkparams = linkparams;
    }

    /**
     * for debugging, this will represent the object
     * @return String
     */
    public String logProps(){
        StringBuilder sb = new StringBuilder();
        sb.append("id: ");
        sb.append(getId());
        sb.append(" | ");
        sb.append("targetType: ");
        sb.append(getTarget_type());
        sb.append(" | ");
        sb.append("targetQuery: ");
        sb.append(getTargetQuery());
        sb.append(" | ");
        for (Map.Entry<String, Object> e : linkparams.entrySet()){
            sb.append(format(" %s:%s |", e.getKey(), e.getValue().toString()));
        }

        return sb.toString();
    }
}
