package com.avellinus.neo4jRedisPlugin.redis.models;

/**
 * A Redis Object
 */
public class RedisObject {
    private String id;

    /**
     * A String id
     * @return String
     */
    public String getId() {
        return id;
    }

    /**
     * Setting the id for the object
     * @param id String
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Setting a long id cast to string
     * @param id String
     */
    public void setId(Long id) {
        this.id = id.toString();
    }

    /**
     * equivalence by id
     */
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null ||
                !(o instanceof RedisEntity)) {

            return false;
        }

        RedisObject other  = (RedisObject) o;

        //

        if (this.getId().equals(((RedisObject) o).getId())){
            return true;
        }else{
            return false;
        }

    }
    /**
     * equivalence by id
     */
    public int hashCode() {
        return this.id.hashCode();
    }
}
