package com.avellinus.neo4jRedisPlugin.redis.models;

import java.util.HashMap;
import java.util.Map;

import static java.lang.String.format;

/**
 * Container for persisting an entity (usually a document)
 * to neo4j via Redis
 */
public class RedisEntity extends RedisObject {
    private Map<String, Object> properties = new HashMap<String, Object>();

    public RedisEntity() {
    }

    /**
     * All kinds of properties can be stored as String - Object
     * map. Please be aware that serialisation via gson can lead
     * to longs being turned to strings.
     *
     * @return Map of Strings to Objects
     */
    public Map<String, Object> getProperties() {
        return properties;
    }

    /**
     * set all kinds of properties as String - Object
     * map. Please be aware that serialisation via gson can lead
     * to longs being turned to strings.
     *
     * @param properties A Mapping of Strings to Objects
     */
    public void setProperties(Map<String, Object> properties) {
        this.properties = properties;
    }

    /**
     * for debugging, this will represent the entity
     * @return String
     */
    public String logProps(){
        StringBuilder sb = new StringBuilder();
        sb.append("id: " + getId() + " | ");
        for (Map.Entry<String, Object> e : properties.entrySet()){
            sb.append(format(" %s:%s |", e.getKey(), e.getValue().toString()));
        }

        return sb.toString();
    }
}
