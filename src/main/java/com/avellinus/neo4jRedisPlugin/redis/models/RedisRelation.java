package com.avellinus.neo4jRedisPlugin.redis.models;

/**
 * Model for persisting a relation between two entities
 * to neo4j via Redis
 */
public class RedisRelation extends RedisObject {
    private Long sourceId;
    private Long targetId;
    private String label;
    private int weight;

    public RedisRelation() {
    }

    /**
     * Long Id of the source entity
     * @return Long id
     */
    public Long getSourceId() {
        return sourceId;
    }

    /**
     * Set the source Id of a relation
     * @param sourceId Long
     */
    public void setSourceId(Long sourceId) {
        this.sourceId = sourceId;
        ((RedisObject) this).setId(""+sourceId + "_" +this.label+ "_"+ targetId);
    }

    /**
     * Set the source id with a string
     * @param sourceId
     */
    public void setSourceId(String sourceId) {
        this.sourceId = Long.valueOf(sourceId).longValue();
        ((RedisObject) this).setId(""+sourceId + "_" +this.label+ "_"+ targetId);
    }

    /**
     * the id of the target entity
     * @return Long
     */
    public Long getTargetId() {
        return targetId;
    }

    /**
     * Set the target id
     * @param targetId Long
     */
    public void setTargetId(Long targetId) {
        this.targetId = targetId;
        ((RedisObject) this).setId(""+sourceId + "_" +this.label+ "_"+ targetId);
    }

    /**
     * set the target id with a string
     * @param targetId
     */
    public void setTargetId(String targetId) {
        this.targetId = Long.valueOf(targetId).longValue();
        ((RedisObject) this).setId(""+sourceId + "_" +this.label+ "_"+ targetId);
    }

    /**
     * the label(s) of the document, splittable by ':'
     * @return String
     */
    public String getLabel() {
        return label;
    }

    /**
     * Setting label & also id of the relation
     * @param label String Label(s) concatenated by ':'
     */
    public void setLabel(String label) {
        this.label = label;
        ((RedisObject) this).setId(""+sourceId + "_" +this.label+ "_"+ targetId);
    }

    /**
     * A weight property indicating the importance of the
     * target for the source
     * @return int
     */
    public int getWeight() {
        return weight;
    }

    /**
     * Setting the importance of the target for
     * the source
     * @param weight
     */
    public void setWeight(int weight) {
        this.weight = weight;
    }

    /**
     * for debugging, this will represent the object
     * @return String
     */
    public String logProps(){
        StringBuilder sb = new StringBuilder();
        sb.append("sourceid: " + getSourceId() + " | ");
        sb.append("targetid: " + getTargetId() + " | ");
        sb.append("label: " + getLabel() + " | ");
        sb.append("weight: " + getWeight() + " | ");

        return sb.toString();
    }


}
