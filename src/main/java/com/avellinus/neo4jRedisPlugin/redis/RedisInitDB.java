package com.avellinus.neo4jRedisPlugin.redis;

import com.avellinus.neo4jRedisPlugin.redis.models.RedisEntity;
import com.avellinus.neo4jRedisPlugin.redis.models.RedisRelation;
import com.avellinus.neo4jRedisPlugin.utlis.DateIterator;
import com.avellinus.neo4jRedisPlugin.utlis.Settings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.logging.Logger;

import static java.lang.String.format;

/**
 * Created by rapps on 28.06.14.
 */
public class RedisInitDB {
    private static Settings settings = Settings.getInstance();
    SimpleDateFormat isoDate = new SimpleDateFormat("yyyyMMdd");
    SimpleDateFormat mDate = new SimpleDateFormat("yyyyMM");
    private Logger logger = Logger.getLogger(RedisInitDB.class.toString());
    //private Jedis jedis = new Jedis(settings.get().getString("database/jedis-srv"));
    private GsonBuilder gb = new GsonBuilder();
    JedisDAO jedisDAO = new JedisDAO();

    public void generateTaxonomyRoot(Map<String, String> entMap) {
        RedisEntity taxRootNode = new RedisEntity();
        taxRootNode.setId(1L);
        taxRootNode.getProperties().put("label", settings.taxonomyLabel);
        taxRootNode.getProperties().put("name", "Taxonomie des Bundesrechts");
        taxRootNode.getProperties().put("id", 0L);
        Gson gson = gb.create();
        entMap.put(settings.entityProdPrefix + taxRootNode.getId(), gson.toJson(taxRootNode, RedisEntity.class));
    }

    public void generateTimeline(Map<String, String> entMap, Map<String, String> relMap) {
        // creating timeline
        String eDate = settings.grafEarliestDate;
        String lDate = settings.grafLatestDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        DateIterator dI = null;
        try {
            dI = new DateIterator(sdf.parse(eDate), sdf.parse(lDate));
        } catch (ParseException e) {
            logger.severe(e.getMessage());
            throw new RuntimeException();
        }
        Long oldDayid = null;
        int iter = 0;
        while (dI.hasNext()) {
            Calendar currentDate = Calendar.getInstance();
            currentDate.setTime(dI.next());
            Long currentId = Long.parseLong(isoDate.format(currentDate.getTime()));
            RedisRelation dayRel = null;
            if (oldDayid != null) {
                dayRel = new RedisRelation();
                dayRel.setSourceId(oldDayid);
                dayRel.setTargetId(currentId);
                dayRel.setLabel(settings.dateFollowingLabel);
                Gson gson = gb.create();
                relMap.put(settings.relProdPrefix + dayRel.getId(), gson.toJson(dayRel, RedisRelation.class));
            }
            generateDateNodes(currentDate, entMap, relMap);
            oldDayid = currentId;
            savePrep2Redis(entMap, relMap);
            iter++;
            if (iter % 1000 == 0) logger.info(format("made %s date nodes", iter));
        }
    }

    public void savePrep2Redis(Map<String, String> entMap, Map<String, String> relMap) {
        boolean done = false;
        while (!done) {
            try {
                jedisDAO.saveContainer(entMap, settings.redisQueue);
                jedisDAO.saveContainer(relMap, settings.redisQueue);
                done = true;
            } catch (Exception e) {
                logger.info("jedis transaction failed, retry");
            }
        }
        entMap.clear();
        relMap.clear();
    }

    public void generateDateNodes(Calendar cal, Map<String, String> dateMap, Map<String, String> relMap) {
        Gson gson = gb.create();
        // make my day
        RedisEntity dayNode = new RedisEntity();
        dayNode.setId(Long.parseLong(isoDate.format(cal.getTime()))); // important for db comparisons!
        dayNode.getProperties().put("nummer", settings.formatDay.format(cal.getTime()));
        dayNode.getProperties().put("name", settings.formatDayFull.format(cal.getTime()));
        String label = format("%s:%s", this.settings.baseDateLabel, settings.dayLabel);
        dayNode.getProperties().put("label", label);
        dateMap.put(settings.entityProdPrefix + dayNode.getId(), gson.toJson(dayNode, RedisEntity.class));

        // make my month
        RedisEntity monthNode = new RedisEntity();
        monthNode.setId(Long.parseLong(mDate.format(cal.getTime())));
        monthNode.getProperties().put("nummer", settings.formatMonth.format(cal.getTime()));
        monthNode.getProperties().put("name", settings.formatMonthFull.format(cal.getTime()));
        label = format("%s:%s", settings.baseDateLabel, settings.monthLabel);
        monthNode.getProperties().put("label", label);
        RedisRelation monthRel = new RedisRelation();
        monthRel.setSourceId(monthNode.getId());
        monthRel.setTargetId(dayNode.getId());
        monthRel.setLabel(settings.defaultDateRelLabel);
        dateMap.put(settings.entityProdPrefix + monthNode.getId(), gson.toJson(monthNode, RedisEntity.class));
        relMap.put(settings.relProdPrefix + monthRel.getId(), gson.toJson(monthRel, RedisRelation.class));

        // make my year
        RedisEntity yearNode = new RedisEntity();
        //Long yearId =MakeId.fromString(humanReadableYear.format(cal.getTime()) + "date");
        yearNode.setId(Long.parseLong(settings.humanReadableYear.format(cal.getTime())));
        yearNode.getProperties().put("nummer", settings.formatYear.format(cal.getTime()));
        yearNode.getProperties().put("name", settings.formatYear.format(cal.getTime()));
        label = format("%s:%s", settings.baseDateLabel, settings.yearLabel);
        yearNode.getProperties().put("label", label);
        RedisRelation yearRel = new RedisRelation();
        yearRel.setSourceId(yearNode.getId());
        yearRel.setTargetId(monthNode.getId());
        yearRel.setLabel(settings.defaultDateRelLabel);
        dateMap.put(settings.entityProdPrefix + yearNode.getId(), gson.toJson(yearNode, RedisEntity.class));
        relMap.put(settings.relProdPrefix + yearRel.getId(), gson.toJson(yearRel, RedisRelation.class));
    }

}
