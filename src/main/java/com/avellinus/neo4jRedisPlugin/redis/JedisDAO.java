package com.avellinus.neo4jRedisPlugin.redis;

import com.avellinus.neo4jRedisPlugin.redis.models.*;
import com.avellinus.neo4jRedisPlugin.utlis.JedisQueue;
import com.avellinus.neo4jRedisPlugin.utlis.JedisTimedQueue;
import com.avellinus.neo4jRedisPlugin.utlis.Settings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import redis.clients.jedis.Jedis;

import java.util.*;
import java.util.logging.Logger;

import static java.lang.String.format;

/**
 * Created by rapps on 24.05.14.
 */

/**
 * Class that can access RedisObjects to produce to Neo4j
 */
public class JedisDAO {
    private static Settings settings = Settings.getInstance();
    private Logger logger = Logger.getLogger(JedisDAO.class.toString());
    private GsonBuilder gb = new GsonBuilder();
    private Map<String, String> entityUpdateContainer = new TreeMap<String, String>();

    /**
     * collecting Redis Updates for later production to queue
     * @param e RedisEntity
     */
    public void collectUpdate(RedisEntity e) {
        String prod_id = format("%s%s", settings.entUpdProdPrefix, e.getId());
        Gson gson = gb.create();
        String content = gson.toJson(e, RedisEntity.class);
        entityUpdateContainer.put(prod_id, content);
    }

    /**
     * Save the String serialized RedisObjects in the container to a queue
     * (mainly for requeuing failed productions)
     */
    public void save(){

        boolean done = false;
        while (!done) {
            try {
                if (entityUpdateContainer.size() > 0) saveContainer(entityUpdateContainer, settings.redisQueue);
                done = true;
            } catch (Exception e) {
                logger.info("jedis transaction failed, retry");
            }
        }

    }

    /**
     * Save single container
     * @param container Containers that String serialized
     *                  RedisObjects are stored in
     * @param queueName The Queue the entities shall be saved to
     *                  (either settings.redisQueue for regular, or
     *                  settings.redisDelayedQueue for timed
     */
    public void saveContainer(Map<String, String> container, String queueName) {
        Jedis jedis = new Jedis(settings.redisSrv, settings.redisSrvPort, 10000);
        try {
            JedisQueue jq = getQueue(queueName, jedis);

            for (Map.Entry<String,String> e : container.entrySet()){
                jq.enqueue(e.getKey().split(":")[0] + ":" + e.getValue());
            }

            container.clear();
        }finally{
            if (null != jedis) {
                jedis.close();
            }

        }
    }

    /**
     * Method to deserialize RedisObjects from a Queue
     *
     * @param batchsize the number of Objects to deserialize at once
     * @param queueName the queue to take the Objects from
     * @return  List of RedisObjects
     */
    public List<RedisObject> getObjectsFromQueue(int batchsize, String queueName){
        Jedis jedis = new Jedis(settings.redisSrv, settings.redisSrvPort, 10000);
        List<RedisObject> jo = new ArrayList<RedisObject>();
        try{

            JedisQueue jc = getQueue(queueName, jedis);
            List<String> jobs = jc.toArray(batchsize);
            for (String job: jobs){
                if (job == null)
                    throw new RuntimeException("jedis read error!");
                String type = job.substring(0, job.indexOf(":")+1);
                String content = job.substring(type.length());
                RedisObject o = null;
                Gson gson = new Gson();
                if (type.equals(settings.entityProdPrefix) || type.equals(settings.entUpdProdPrefix)){
                    o = gson.fromJson(content, RedisEntity.class);
                }else if (type.equals(settings.relProdPrefix)) {
                    o = gson.fromJson(content, RedisRelation.class);
                }else if (type.equals(settings.updProdPrefix)){
                    o = gson.fromJson(content, RedisLink.class);
                }
                jo.add(o);
            }

        }finally{
            if (null != jedis) {
                jedis.close();
            }

        }
        return  jo;
    }

    /**
     * Pop a specified amount of Objects from a specified
     * queue
     *
     * @param amount int
     * @param queueName one of the two queue types
     */
    public void pop(int amount, String queueName){
        Jedis jedis = new Jedis(settings.redisSrv, settings.redisSrvPort, 10000);
        try{

            JedisQueue jc = getQueue(queueName, jedis);
            jc.dequeue(amount);

        }finally{
            if (null != jedis) {
                jedis.close();
            }

        }
    }

    /**
     * get a status to log from a queue
     * @param queueName the queue the info is asked for
     * @return A log string
     */
    public String logQueueStatus(String queueName){
        Jedis jedis = new Jedis(settings.redisSrv, settings.redisSrvPort, 10000);
        String msg;
        StringBuilder sb = new StringBuilder();
        try{
            JedisQueue jc = getQueue(queueName, jedis);
            sb.append(" objects: ");
            sb.append(jc.size());
            sb.append(" queued");
            sb.append(" in ");
            sb.append(queueName);
            msg = sb.toString();
            jc = null;
        }finally{
            if (null != jedis) {
                jedis.close();
            }

        }
        return msg;
    }

    /**
     * Get Queue length
     * @param queueName the name of the queue
     * @return int
     */
    public int queueLength(String queueName){
        Jedis jedis = new Jedis(settings.redisSrv, settings.redisSrvPort, 10000);
        int len;
        StringBuilder sb = new StringBuilder();
        try{
            JedisQueue jc = getQueue(queueName, jedis);
            len = jc.size();

        }finally{
            if (null != jedis) {
                jedis.close();
            }

        }
        return len;
    }

    /**
     * increment a failed value for a production.
     *
     * @param prefix the production prefix of the failed RedisObject
     *               see settings.(...)Prefix for possible values
     * @param id the long id (serialized as string) of the failed Object
     * @param queueName The queue where the failed info is stored
     *                  usually either settings.redisFailedSet or
     *                  settings.redisFailedLinks
     *
     */
    public void incrFailed(String prefix, String id, String queueName){
        Jedis jedis = new Jedis(settings.redisSrv, settings.redisSrvPort, 10000);
        try {
            if (jedis.zscore(queueName, prefix + id) == null){
                jedis.zadd(queueName, 1 , prefix + id);
            }else{
                jedis.zincrby(queueName, 1, prefix + id);
            }
        }finally{
            if (null != jedis) {
                jedis.close();
            }

        }
    }

    /**
     * remove an Object representation from a failed queue, if sucessfully
     * produced
     *
     * @param prefix the production prefix of the failed RedisObject
     *               see settings.(...)Prefix for possible values
     * @param id the long id (serialized as string) of the Object
     * @param queueName The queue where the failed info is stored
     *                  usually either settings.redisFailedSet or
     *                  settings.redisFailedLinks
     */
    public void removeFailed(String prefix, String id, String queueName){
        Jedis jedis = new Jedis(settings.redisSrv, settings.redisSrvPort, 10000);
        try {
            jedis.zrem(queueName,prefix + id);
        }finally{
            if (null != jedis) {
                jedis.close();
            }

        }
    }

    /**
     *
     * @param prefix the production prefix of the failed RedisObject
     *               see settings.(...)Prefix for possible values
     * @param id the long id (serialized as string) of the Object
     * @param queueName The queue where the failed info is stored
     *                  usually either settings.redisFailedSet or
     *                  settings.redisFailedLinks
     * @return boolean - true means failed finally
     */
    public boolean checkFailed(String prefix, String id, String queueName){
        Jedis jedis = new Jedis(settings.redisSrv, settings.redisSrvPort, 10000);
        boolean failed = false;
        try {
            if (jedis.zscore(queueName, prefix + id) > settings.retryProduction) failed = true;
            else failed = false;

        }catch(NullPointerException e) {
            failed = false;
        }finally{
            if (null != jedis) {
                jedis.close();
            }
        }
        return failed;
    }

    /**
     * Reserialize and requeue an Object if production was failing
     * of if a Redis Object was created
     *
     * @param o RedisObject to be requeued
     * @param queueName the Queue to put the Object to
     */
    public void requeue(RedisObject o, String queueName){
        Gson gson = gb.create();
        Map<String, String> storeContainer = new HashMap<String, String>();
        String content = "";
        String id = "";
        if (o instanceof RedisEntity){
            content = gson.toJson(o, RedisEntity.class);
            id = settings.entityProdPrefix + o.getId();
        }else if (o instanceof RedisRelation){
            content = gson.toJson(o, RedisRelation.class);
            id = settings.relProdPrefix + o.getId();
        }else if (o instanceof RedisLink){
            content = gson.toJson(o, RedisLink.class);
            id = settings.updProdPrefix + o.getId();
        }
        storeContainer.put(id, content);
        saveContainer(storeContainer, queueName);
    }


    /**
     * careful with this method you need a prefix and a serialized object as ḱey
     * also with regular (List) Queues like redisQueue it will remove only the
     * first occurrence of the key. With set Queues (Timed Queue) it works more
     * like you would expect
     * @param key the redis key to evict
     * @param queueName the queue to evict from
     */
    public void removeFromQueue(String key, String queueName){
        JedisQueue jq;
        Jedis jedis = new Jedis(settings.redisSrv, settings.redisSrvPort, 10000);

        try {
            jq = getQueue(queueName, jedis);
            jq.dequeue(key);
        }finally{
            if (null != jedis) {
                jedis.close();
            }
        }

    }

    /**
     * get a Queue by name. When getting a timed queue
     * a standard delay of two minutes is assumed
     * @param queueName the name of the queue
     * @param jedis a Redis/jedis instance the queue is on
     * @return the queue
     */
    private JedisQueue getQueue(String queueName, Jedis jedis){
        JedisQueue jq;

        if (queueName.equals(settings.redisTimedQueue)){
            jq = getTimedQueue(jedis, queueName, 1000L * 60L * 2L);
        }else{
            jq = new JedisQueue(jedis,  queueName);
        }

        return jq;

    }

    /**
     * get a timed Queue with an arbirtary delay
     * @param jedis a Redis/jedis instance the queue is on
     * @param queueName the name of the queue
     * @param delayMillis the delay
     * @return the queue
     */
    private JedisQueue getTimedQueue(Jedis jedis, String queueName, Long delayMillis){
        JedisQueue jq;
        return new JedisTimedQueue(jedis, queueName, System.currentTimeMillis() + delayMillis);
    }

    /**
     * Producer and Listener will lock, to avoid piling. This is needed to
     * persist the lock to redis
     *
     * @param classToCheck The class that is locking
     * @return boolean
     */
    public boolean PIDlocked(Class classToCheck) {
        Jedis jedis = new Jedis(settings.redisSrv, settings.redisSrvPort, 10000);
        try {
            if (jedis.get(classToCheck.getSimpleName()) == null) return false;
            if (jedis.get(classToCheck.getSimpleName()).equals("running")){
                return true;
            }else{
                return false;
            }
        }finally{
            if (null != jedis) {
                jedis.close();
            }

        }
    }

}
