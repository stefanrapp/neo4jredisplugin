package com.avellinus.neo4jRedisPlugin;
import com.avellinus.neo4jRedisPlugin.neo4j.NeoDaemon;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.server.plugins.*;

import java.util.logging.Logger;

/**
 * Server plugin for neo4j to produce mmetadta from
 * a redis queue to Neo4j
 */
@Description( "Extension to control importer daemon" )
public class plugin extends ServerPlugin {
    Thread daemon;
    NeoDaemon neoDaemon;
    private Logger logger = Logger.getLogger("com.avellinus.neo4j.plugins");

    /**
     * Starts production to Neo4j from
     * Redis-queue
     *
     * @param graphDb Neo4j instance
     * @return json
     */
    @Name( "start_importer_daemon" )
    @Description( "Start reading lnat_data from redis instance" )
    @PluginTarget( GraphDatabaseService.class )
    public String getStart( @Source GraphDatabaseService graphDb )
    {
        if (neoDaemon == null){
            neoDaemon = new NeoDaemon(graphDb);
            daemon = new Thread(neoDaemon);
            daemon.setDaemon(true);
            daemon.start();
            return "started importer daemon";
        } else{
            return "already running";
        }
    }

    /**
     * Stops production from redis-queue
     *
     * @param graphDb Neo4j instance
     * @return json
     */
    @Name("stop_importer_daemon")
    @Description( "End reading lnat_data from redis instance" )
    @PluginTarget( GraphDatabaseService.class )
    public String getEnd( @Source GraphDatabaseService graphDb )
    {
        neoDaemon.stopService();
        try{
            neoDaemon.wait();
            daemon.interrupt();
        }catch(Exception e){
            logger.severe("stopped");
            neoDaemon = null;
            daemon = null;
        }
        return "ended importer daemon";
    }

    /**
     * shows plugin status via REST call
     *
     * @param graphDb Neo4j instance
     * @return json
     */
    @Name("status_importer_daemon")
    @Description( "status lnat_data from redis instance" )
    @PluginTarget( GraphDatabaseService.class )
    public String getStatus( @Source GraphDatabaseService graphDb )
    {
        String msg;
        if (neoDaemon == null) msg = "status: stopped";
        try {
            msg = neoDaemon.getQueueStatus();

        } catch (NullPointerException e) {
            msg = "status: busy";
        }
        return msg;
    }

}
