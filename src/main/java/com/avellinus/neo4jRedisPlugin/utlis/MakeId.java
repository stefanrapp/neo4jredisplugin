package com.avellinus.neo4jRedisPlugin.utlis;

import com.google.common.base.Charsets;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

/**
 * Method that provides IDs as used in the db. Metadata IDs (no path available) are
 * Done in a static method in the models Object
 *
 * path is split after "lexxml" - slashes are removed to make it
 * independent from platform and location of lexxml
 *
 */
public class MakeId {

    /**
     * will try to normalize the lexxml path to provide
     * same ids
     *
     * @param path a String path that is bound to provide a long id
     * @return a normalized path string
     */
    public synchronized static String normalizeLexxmlPath(String path){
        if (path.contains("lexxml")){
            path = path.split("lexxml")[1];
        }
        path = path.replace("\\", "/");
        return path;
    }

    /**
     * provides the first half of a MD5 to id a path or an other
     * string in an efficient way
     *
     * @param identifier String - most often a path
     * @return long ID
     */
    public synchronized static Long fromString(String identifier){
        identifier = normalizeLexxmlPath(identifier);
        HashFunction hf = Hashing.md5();
        Long hc = null;
        hc = hf.newHasher()
                .putString(identifier, Charsets.UTF_8)
                .hash()
                .asLong();
        return hc;
    }
}
