package com.avellinus.neo4jRedisPlugin.utlis;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Class to store default values and constants for plugin
 */
public class Settings {
    private static Settings ourInstance = new Settings();

    /**
     * General settings
     */

    // dates, that exceeding dates in data will get reduced to
    public static String grafEarliestDate="1800-01-01";
    public static String grafLatestDate="2070-01-01";


    /**
     * Date Formats and general regex for parsing
     */
    public static SimpleDateFormat formatDay = new SimpleDateFormat("dd");
    public static SimpleDateFormat formatDayFull = new SimpleDateFormat("EEEEEE", Locale.GERMAN);
    public static SimpleDateFormat formatMonth = new SimpleDateFormat("MM");
    public static SimpleDateFormat formatMonthFull= new SimpleDateFormat("MMMMMMM", Locale.GERMAN);
    public static SimpleDateFormat formatYear= new SimpleDateFormat("yyyy");
    public static SimpleDateFormat humanReadableMonth = new SimpleDateFormat("yyyy-MM");
    public static SimpleDateFormat humanReadableYear =  new SimpleDateFormat("yyyy");
    public static SimpleDateFormat isoDate = new SimpleDateFormat("yyyyMMdd");
    public static SimpleDateFormat mDate = new SimpleDateFormat("yyyyMM");

    /**
     * Redis/Jedis config
     */
    // redis server location
    public static int redisSrvPort = 6379;
    public static String redisSrv = "localhost";

    //redis queue/prefixin values
    public static String redisQueue ="redis-queue";
    public static String redisDelayedQueue ="redis-delayed-queue";
    public static String redisFailedSet ="redis-failed-set";
    public static String redisFailedLinks="redis-failed-links";
    public static String redisTimedQueue ="redis-timed-queue";
    public static String docIdProductionPrefix="did-path:";
    public static String relProdPrefix = "rel-id:";
    public static String entityProdPrefix ="ent-id:";
    public static String entUpdProdPrefix ="ent-upd-id:";
    public static String updProdPrefix="upd-id:";

    public static String groissPathDeletePrefix="dl-path:";
    public static String groissPathVersionPrefix="vn-path:";

    // number of retries to produce before giving up on a redis object
    public static int retryProduction = 5;

    /**
     * Neo4j --
     */

    // for debugging only ...
    public static String neoDBPath = "/home/rapps/neo4j-srv/data/graph.db";

   // todo: prbly obsolete or/and incomplete?;
    public static String[] entityLabels = {
            "Dokument",
            "Datum",
            "Tag",
            "Monat",
            "Jahr",
            "Stichwort",
            "Volltextstichwort",
            "Verfasser",
            "Taxonomie",
            "Steuerrichtlinie",
            "Zeitschrift",
            "Handbuch",
            "Kommentar",
            "Einzelvorschrift",
            "Gesetz",
    };


    // Autoindex
    public static String[] nodeProps2Index ={ "id",
            "doc_id",
            "linktarget",
            "rechtssatz_nummer",
            "fundstelle",
            "commonlinktarget",
            "linktarget_range_to",
            "linktarget_range_from",
            "path",
            "risgesnr",
            "versionnr"};
    public static String[] relProps2Index ={"id"};

    // entity Labels translation
    public static String baseDateLabel ="Datum";
    public static String dayLabel = "Tag";
    public static String monthLabel = "Monat";
    public static String yearLabel ="Jahr";
    public static String taxonomyLabel ="Taxonomie";

    // rel types translation
    public static String generalCiteLabel ="zitiert";
    public static String defaultDateRelLabel ="enthält";
    public static String dateFollowingLabel="nächster";
    public static String paraFollowingLabel="novelliert";

    public static Settings getInstance() {
        return ourInstance;
    }

    private Settings() {


    }




}


