package com.avellinus.neo4jRedisPlugin.utlis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisDataException;

import java.util.ArrayList;
import java.util.List;


/**
 * FIFO SET with Redis/Jedis
 */
public class JedisTimedQueue extends JedisQueue {

    private transient Jedis jedis;
    private long earliestProduction;
    private String pattern;

    /**
     * @param jedis
     * @param pattern
     */
    public JedisTimedQueue(Jedis jedis, String pattern, long earliestProduction) {
        super(jedis, pattern);
        this.pattern = pattern;
        this.jedis = jedis;
        this.earliestProduction = earliestProduction;
    }

    /**
     * Clear out the values for this.pattern
     */
    public void clear() {
        this.jedis.del(this.pattern);

    }

    /**
     * @return whether this.pattern maps to no values
     *
     */
    public boolean isEmpty() {
        return (this.size() == 0);
    }


    /**
     * @return the number of values for this.pattern
     */
    public int size() {
        return new Integer(this.jedis.zcard(this.pattern).toString());
    }


    /**
     * @return
     *  serialized form of the Jedis queue
     */
    public List<String> toArray(int amount) {
        return new ArrayList(this.jedis.zrangeByScore(this.pattern, 0, System.currentTimeMillis(), 0, amount));
    }

    /**
     * @param elems the element(s) to add
     */
    public void enqueue(String... elems) {
        for (String elem : elems){
            this.jedis.zadd(this.pattern, earliestProduction, elem);
        }

    }

    /**
     * @return the next item in the queue, popped
     *
     */
    public void dequeue(int amount) {
        try {
            this.jedis.zrevrangeByScore(this.pattern, 0, earliestProduction, 0, amount);
        }
        catch (JedisDataException e) {
            // It wasn't a list of strings
        }
    }

    /**
     * Careful, you need the serialized object plus prefix to delete
     * @param elems
     */
    public void dequeue(String... elems) {
        for (String elem: elems){
            this.jedis.zrem(this.pattern, elems);
        }
    }


}