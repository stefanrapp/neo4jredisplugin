package com.avellinus.neo4jRedisPlugin.utlis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisDataException;

import java.util.List;


/**
 * FIFO with Redis/Jedis
 */
public class JedisQueue {

    private transient Jedis jedis;

    private final String pattern;

    /**
     * @param jedis
     * @param pattern
     */
    public JedisQueue(Jedis jedis, String pattern) {
        this.jedis = jedis;
        this.pattern = pattern;
    }

    /**
     * Clear out the values for this.pattern
     */
    public void clear() {
        this.jedis.del(this.pattern);

    }

    /**
     * @return whether this.pattern maps to no values
     *
     */
    public boolean isEmpty() {
        return (this.size() == 0);
    }


    /**
     * @return the number of values for this.pattern
     */
    public int size() {
        return new Integer(this.jedis.llen(this.pattern).toString());
    }


    /**
     * @return
     *  serialized form of the Jedis queue
     */
    public List<String> toArray(int amount) {
        return this.jedis.lrange(this.pattern, 0, amount);
    }

    /**
     * @param elems the element(s) to add
     */
    public void enqueue(String... elems) {
        this.jedis.rpush(this.pattern, elems);
    }

    /**
     * @return the next item in the queue, popped
     *
     */
    public void dequeue(int amount) {
        try {
            this.jedis.ltrim(this.pattern, amount, -1);
        }
        catch (JedisDataException e) {
            // It wasn't a list of strings
        }
    }

    /**
     * careful, this only removes the first occurence in the list!
     * also: most queues contain serialized object plus some identifier
     * prefix - deuque via String works only with those
     * @param elems
     */
    public void dequeue(String... elems) {
        try {
            for (String elem: elems){
                this.jedis.lrem(elem, 1, elem);
            }

        }
        catch (JedisDataException e) {
            // It wasn't a list of strings
        }
    }
}