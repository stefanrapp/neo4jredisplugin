package com.avellinus.neo4jRedisPlugin.neo4j;

import com.avellinus.neo4jRedisPlugin.redis.JedisDAO;
import com.avellinus.neo4jRedisPlugin.redis.models.RedisObject;
import com.avellinus.neo4jRedisPlugin.utlis.Settings;
import org.neo4j.graphdb.GraphDatabaseService;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static java.lang.String.format;

/**
 * The class that monitors redis and controls
 * production to Neo4j
 */
public class NeoDaemon implements Runnable{
    private static Logger logger = Logger.getLogger(NeoDaemon.class.toString());
    private GraphDatabaseService db;
    private boolean stop = false;
    private static NeoDAO nDAO;
    private static JedisDAO jDAO;
    private Settings settings = Settings.getInstance();

    /**
     * Instantiate neo4j
     *
     * @param graphDb an Instance of Neo4j
     */
    public NeoDaemon(GraphDatabaseService graphDb) {
        db = graphDb;
    }

    /**
     * will stop production via REST call
     */
    public void stopService() {
        this.stop = true;
    }

    /**
     * check queue sizes to decide which kinds of objects to produce
     *
     * redisQueue: regular Queue for all entities that do not rely on other
     * entries to be able to produce - e.g. entites
     *
     * redisDelayedQueue: A Queue that relies on other information that may
     * not be present at the time of production (yet) - this is used for links between
     * documents (will not run when parsing job is running)
     *
     * redisTimedQueue: A Queue that will only return objects to produce after an
     * arbitrary amount of milliseconds have passed to wait for another process
     * to deliver metadata e.g. a webservice call to another resource where the
     * object in question has to be produced itself before meta info is available
     *
     */
    public void run() {
        nDAO = new NeoDAO(db);
        jDAO = new JedisDAO();
        while(! stop) {

            if (jDAO.queueLength(settings.redisQueue) != 0){
                produce(settings.redisQueue);
            }else if (jDAO.queueLength(settings.redisDelayedQueue) != 0 && ! jDAO.PIDlocked(Process.class)) {
                produce(settings.redisDelayedQueue );
            }else if (jDAO.getObjectsFromQueue(1, settings.redisTimedQueue).size() > 0 ){
                // not implemented
            }else{
                waitSeconds(60);
            }
        }
    }

    /**
     * Producing redisObjects to Neo4j is started here
     * @param queueName either one onf the queues of the run() method
     */
    public void produce(String queueName){
        int batchsize = 999;


        List<RedisObject> batch = jDAO.getObjectsFromQueue(batchsize, queueName);
        if (batch.size() > 0) {
            logger.info("producing " + batch.size() + " of " + jDAO.queueLength(queueName) + " of " + queueName );
            nDAO.save(batch);
            jDAO.pop(batch.size(), queueName);
        }
    }

    /**
     * queue status for logging
     * @return String for logging
     */
    public String getQueueStatus() {

        if (this.stop) return "stopped";

        return jDAO.logQueueStatus(settings.redisQueue) +
                jDAO.logQueueStatus(settings.redisDelayedQueue);
    }

    /**
     * waiting if idle
     * @param sec int value for seconds to wait
     */
    private void waitSeconds(int sec) {
        logger.info(format("waiting - %d seconds for input", sec));
        try {
            Thread.sleep(sec * 1000); // wait a minute
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
