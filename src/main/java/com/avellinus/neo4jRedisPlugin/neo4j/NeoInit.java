package com.avellinus.neo4jRedisPlugin.neo4j;

import com.avellinus.neo4jRedisPlugin.redis.RedisInitDB;
import com.avellinus.neo4jRedisPlugin.redis.models.RedisEntity;
import com.avellinus.neo4jRedisPlugin.redis.models.RedisRelation;
import com.avellinus.neo4jRedisPlugin.utlis.Settings;
import org.neo4j.graphdb.*;
import org.neo4j.graphdb.index.AutoIndexer;
import org.neo4j.graphdb.index.ReadableIndex;
import org.neo4j.graphdb.schema.ConstraintDefinition;

import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

/**
 * Class to initialize a Neo4j Instance, writing
 * a timeline and a taxonomy root, as well as
 * constraints and indices
 *
 */
public class NeoInit {
    private static Logger logger = Logger.getLogger(NeoInit.class.toString());
    private static Settings settings = Settings.getInstance();

    /**
     * check if db is already initialized
     * @param db The neo4j instance to check
     * @param autoNodeIndex the legacy auto node index
     * @return boolean
     */
    public static boolean dbIsLive(GraphDatabaseService db, ReadableIndex<Node> autoNodeIndex) {
        Iterable<ConstraintDefinition> n = null;
        try (Transaction tx = db.beginTx()) {
            n = db.schema().getConstraints();
            if (n.iterator().hasNext() && autoNodeIndex.get("id", 20700101L) != null) {
                return true;
            }
            tx.success();
        }

        return false;
    }

    /**
     * Method to initialize a neo4j instance if it is not yet done
     */
    protected static void prepDB() {
        logger.info("prepping DB for first use... (may take a while)");
        // creating taxonomy root & timeline
        Map entMap = new TreeMap<String, RedisEntity>();
        Map relMap = new TreeMap<String, RedisRelation>();
        RedisInitDB init = new RedisInitDB();
        init.generateTaxonomyRoot(entMap);
        init.generateTimeline(entMap, relMap);
    }

    /**
     * Method to initialize the legacy auto index. The legacy auto index
     * is used because it allows lucene queries via Java API and Cypher.
     * Esp. range queries were lacking from the new index system at the
     * time of writing
     *
     * @param db the neo4j instance
     */
    protected static void addAutoIndices(GraphDatabaseService db) {
        try (Transaction tx = db.beginTx()) {
                AutoIndexer<Node> nodeAutoIndexer = db.index().getNodeAutoIndexer();
                AutoIndexer<Relationship> relAutoIndexer = db.index().getRelationshipAutoIndexer();
                for (String prop: settings.nodeProps2Index){
                    nodeAutoIndexer.startAutoIndexingProperty(prop);
                }
                for (String prop: settings.relProps2Index) {
                    relAutoIndexer.startAutoIndexingProperty(prop);
                }
                nodeAutoIndexer.setEnabled(true);
                relAutoIndexer.setEnabled(true);
            tx.success();
        }
    }

    /**
     * This method should be run once on a new db, to add unique constraints, build inices etc
     */
    protected static void addContstraints(GraphDatabaseService db, String[] entityLabels) {

        try (Transaction tx = db.beginTx()) {
            Iterable<ConstraintDefinition> constr = db.schema().getConstraints();
            if (!constr.iterator().hasNext()) {
                for (String labelString : entityLabels) {
                    Label label = DynamicLabel.label(labelString);
                    db.schema()
                            .constraintFor(label)
                            .assertPropertyIsUnique("id")
                            .create();
                }
            }
            tx.success();
        }
    }

}
