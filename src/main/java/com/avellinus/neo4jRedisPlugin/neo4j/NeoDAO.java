package com.avellinus.neo4jRedisPlugin.neo4j;

import com.avellinus.neo4jRedisPlugin.redis.JedisDAO;
import com.avellinus.neo4jRedisPlugin.redis.models.*;
import com.avellinus.neo4jRedisPlugin.utlis.Settings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.neo4j.graphdb.*;
import org.neo4j.graphdb.index.ReadableIndex;

import java.sql.Timestamp;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by rapps on 12.05.14.
 */
public class NeoDAO {
    private Logger logger = Logger.getLogger(NeoDAO.class.toString());
    private GraphDatabaseService db;
    private Settings settings = Settings.getInstance();
    private JedisDAO jDAO = new JedisDAO();
    private ReadableIndex<Node> autoNodeIndex;
    private ReadableIndex<Relationship> autoRelIndex;
    private GsonBuilder gb = new GsonBuilder();

    /**
     * Start the db, and initialize it, if timeline, constraints
     * and/or Indices are not present (first run)
     *
     * @param db Instance of Neo4j
     */
    public NeoDAO(GraphDatabaseService db) {
        this.db = db;

        autoNodeIndex = this.db.index()
                .getNodeAutoIndexer()
                .getAutoIndex();

        autoRelIndex = this.db.index()
                .getRelationshipAutoIndexer()
                .getAutoIndex();



        if (!NeoInit.dbIsLive(this.db, this.autoNodeIndex)) {

            NeoInit.addContstraints(this.db, settings.entityLabels);

            NeoInit.prepDB();
        }

        // init the legacy Indices in any case
        NeoInit.addAutoIndices(this.db);
    }

    /**
     * this method is used to split the batch of Redis Objects as
     * received by the Daemon process and to persist its items
     *
     * @param batch a list of RedisObjects that should be written
     *              to neo4j
     */
    public void save(List<RedisObject> batch) {

        // making separate lists for saving/updating etc and deleting
        List<List<RedisObject>> l = groupRedisObjects(batch);

        RedisObject o;
        for (List<RedisObject> sublist : l){

            try (Transaction tx = db.beginTx()) {
                for (int i = 0; i <sublist.size(); i++) {

                    o = sublist.get(i);

                    if (o instanceof RedisEntity) {
                        saveOrUpdateNode((RedisEntity) o);
                    } else if (o instanceof RedisRelation) {
                        saveOrUpdateRelation((RedisRelation) o);
                    }else if (o instanceof RedisLink) {
                        update((RedisLink) o);
                    }
                }

                tx.success();

            } catch (Exception e) {
                String cause = "";
                if (e.getCause() != null){
                    cause = ""+e.getCause();
                    if (e.getCause().getCause() != null){
                        cause += "|" + e.getCause().getCause();
                    }
                }
                logger.severe("failed neo transaction cause was: " + e.toString());
                throw new RuntimeException(e.getMessage());

            }
        }

    }

    /**
     * method that will split a batch of Redis Objects in Lists of save/update
     * and delete objects - with an active production queue, saves, deletes and
     * resaves can happen in quick succession, making it paraqmount to tansact
     * deletes and saves in the correct sequence.
     *
     * @param input the input batch of mixed RedisObjects
     * @return List of Lists of either deletes or save/update batches
     */
    private List<List<RedisObject>> groupRedisObjects(List<RedisObject> input){
        List<List<RedisObject>> result = new ArrayList<List<RedisObject>>();
        List<RedisObject> intermediaryList = new ArrayList<RedisObject>();
        String newClass = "";
        String oldClass = "";
        for (RedisObject o : input) {
            try {
                if (o.getClass().getSimpleName().contains("Delete")) newClass = "del";
                else newClass = "saveOrUpd";
                if (newClass.equals(oldClass)) {
                    intermediaryList.add(o);
                } else {
                    if (intermediaryList.size() > 0) {
                        result.add(intermediaryList);
                    }
                    intermediaryList = new ArrayList<RedisObject>();
                    intermediaryList.add(o);
                    oldClass = newClass;
                }
            } catch (Exception e) {
                logger.severe(""+e.getStackTrace());
                        logger.severe( e.getCause() + " "+ e.getMessage());
            }
        }
        if (intermediaryList.size() > 0) {
            result.add(intermediaryList);
        }
        return result;
    }

    /**
     * Persisting/updating regular RedisEntities (all Document objects from
     * the queues except legislation) this Method MUST be called in an open
     * transaction
     *
     * @param entity the RedisEntity to be persisted
     */
    private void saveOrUpdateNode(RedisEntity entity) {
        if (entity.getProperties().containsKey("lupdate")){
            System.out.print("");
        }else{
            entity.getProperties().put("lupdate", (new Timestamp((new Date()).getTime())).toString());
        }
        entity.getProperties().remove("id"); // sometime this gets passed as param
        boolean isDocIDupdate= entity.getProperties().containsKey("doc_id") && !entity.getProperties().containsKey("label");

        //check existing
        Node oldNode = this.autoNodeIndex.get("id", entity.getId()).getSingle();

        if (oldNode == null) {
            // has not been produced yet

            if (isDocIDupdate){
                // mark failed - docid update without the entity to assign it to
                check(entity, "found no node for doc_id: " + entity.getProperties().get("doc_id"), settings.entUpdProdPrefix);
            }else{
                // regular save
                persist(entity, settings.entUpdProdPrefix);
            }

        } else { // exists - update

            update(entity, oldNode, settings.entUpdProdPrefix);

        }
    }

    /**
     * persists a relationship between two entity/nodes in neo4j
     * when updating, weights will be added (!)
     *
     * @param relation
     */
    private void saveOrUpdateRelation(RedisRelation relation) {

        // check existing in Neo
        Relationship oldRel = this.autoRelIndex.get("id", relation.getId()).getSingle();

        if (oldRel == null) { // Relation unknown
            boolean existingReference = false;

            Node srcNode = null;
            Node tgtNode = null;

            srcNode = this.autoNodeIndex.get("id", relation.getSourceId()).getSingle();
            tgtNode = this.autoNodeIndex.get("id", relation.getTargetId()).getSingle();

            existingReference = (srcNode != null && tgtNode != null) ? true : false;

            if (!existingReference) { // lacking reference
                if (srcNode == null){
                    check(relation, "relation lacking sourceNode src:" +relation.getSourceId(), settings.relProdPrefix);
                }else{
                    check(relation, "relation lacking targetNode src:" +relation.getSourceId(), settings.relProdPrefix);
                }

                return;
            } else { // new relation, reference present -create
                DynamicRelationshipType relType = DynamicRelationshipType.withName(relation.getLabel());
                Relationship resultRel = srcNode.createRelationshipTo(tgtNode, relType);
                if (relation.getWeight() != 0) {
                    resultRel.setProperty("weight", relation.getWeight());
                }
                resultRel.setProperty("id", relation.getId());
                jDAO.removeFailed(settings.relProdPrefix, relation.getId(), settings.redisFailedSet);
            }
        } else {
            int oldRelWeight = 0;
            if (oldRel.hasProperty("weight")){
                Object o = oldRel.getProperty("weight");
                if (o instanceof Integer){
                    oldRelWeight = (Integer) o;
                }else if (o instanceof Double){
                    oldRelWeight = ((Double)o).intValue() ;
                }
                // will not be increased outside fullreloads (no cascading decreases)
                oldRel.setProperty("weight", oldRelWeight + relation.getWeight());
            }
            oldRel.setProperty("id", relation.getId());
            jDAO.removeFailed(settings.relProdPrefix, relation.getId(), settings.redisFailedSet);
        }

    }

    /**
     * Method to persist Links in Neo4j, Multiple linktargets are
     * possible and will be represented by a division of weight
     *
     * @param update a RedisLink Object to be persisted
     */
    private void update(RedisLink update) {
        try{

            List<Node> tgtNodes = new ArrayList<Node>();
            int multi = 0;
            Node srcNode = this.autoNodeIndex.get("id", update.getSource_id()).getSingle();

            if (srcNode == null){ // skip & requeue if src not (yet) present
                String message ="linking failed - id "+update.getSource_id()+" not present";
                check(update, message, settings.updProdPrefix);
                return;

            }else{
                String query = update.getTargetQuery();
                Iterator<Node> r = this.autoNodeIndex.query(query);
                while (r.hasNext()){
                    tgtNodes.add(r.next());
                }
                multi = tgtNodes.size();
            }

            if (multi ==0){ // skip if target not found
                check(update, "target not found: " + update.getTargetQuery(), settings.updProdPrefix);

            }else{

                DynamicRelationshipType relType = DynamicRelationshipType.withName(settings.generalCiteLabel);

                for (Node tgt : tgtNodes){

                    String rel_id = update.getSource_id() + "_" + settings.generalCiteLabel + "_" + tgt.getProperty("id");
                    Relationship oldRel = null;
                    try{
                        oldRel = this.autoRelIndex.get("id", rel_id).getSingle();
                    }catch(Exception e){
                        logger.severe(e.getCause() + " " + e.getMessage());
                    }
                    Relationship resultRel = null;

                    if (oldRel == null){ //new rel, assign id
                        resultRel = srcNode.createRelationshipTo(tgt, relType);
                        resultRel.setProperty("id", rel_id);

                    }else{ //existing - just update
                        resultRel = oldRel;
                    }

                    for (Map.Entry<String, Object> e : update.getLinkparams().entrySet()){
                        resultRel.setProperty(e.getKey(), e.getValue());
                    }
                    if (multi > 1){
                        resultRel.setProperty("multi", multi);
                    }


                }

                jDAO.removeFailed(settings.updProdPrefix, update.getId(), settings.redisFailedLinks);

            }

        }catch (Exception e){
            logger.severe(e.getCause() + " " + e.getMessage());
        }
    }

    /**
     * Method to check if an RedisObject has been unsuccessfully tried to be persisted,
     * logging the problem and ejecting from queue
     *
     * @param ro a RedisObject to check for
     * @param message
     */
    private void check(RedisObject ro, String message, String proPrefix){
        jDAO.incrFailed(settings.updProdPrefix, ro.getId(), settings.redisFailedLinks);

        if (!jDAO.checkFailed(settings.updProdPrefix, ro.getId(), settings.redisFailedLinks)) {

            jDAO.requeue(ro, settings.redisDelayedQueue);

        } else {
            logger.warning(message + " tried several times.");
            jDAO.removeFailed(settings.updProdPrefix, ro.getId(), settings.redisFailedLinks);

        }

    }

    /**
     * method will strip a neo4j node/entity of all its
     * properties
     * @param n the node that shall be stripped
     * @return the stripped node
     */
    private Node clearProperties(Node n){
        for (String key : n.getPropertyKeys()){
            if (!key.equals("id")) n.removeProperty(key);
        }
        // todo: testing indices!
        return n;
    }

    /**
     * Method to persist a redisObject (Entity or Version)
     * to a neo4j Object
     * @param ro RedisEntity allowed
     * @param productionPrefix settings.entUpdProdPrefix
     * @return the persisted Node os java Object
     */
    private Node persist (RedisObject ro, String productionPrefix){
        Node n = db.createNode(getLabelsFromObject(ro));
        n.setProperty("id", ro.getId());
        n = update(ro, n,productionPrefix);
        return n;
    }

    /**
     * Reassigning properties to an existing node - taking
     * care to persist the shortest full_text property (used with Keywords)
     * @param ro RedisEntity allowed
     * @param n The node that will be updated
     * @param productionPrefix settings.entUpdProdPrefix
     * @return the updated Node
     */
    private Node update (RedisObject ro, Node n, String productionPrefix){
        Map<String,Object> properties = getPropertiesFromObject(ro);
        for (String key: properties.keySet()){
            if (key.equals("full_text") && n.hasProperty("full_text")){
                //exceptional handling of retaining best fulltext keyword
                String oldkeyword = (String)n.getProperty("full_text");
                String newKeyword = (String)properties.get("full_text");
                String fulltext = (oldkeyword.length() <= key.length())? oldkeyword:newKeyword;
                n.setProperty(key, fulltext);
            }else{
                n.setProperty(key, properties.get(key));
            }
        }

        jDAO.removeFailed(productionPrefix, ""+properties.get("id"), settings.redisFailedSet);
        return n;
    }


    /**
     * Helper Method to draw Properies Map from RedisObject
     * @param ro allowed: RedisEntity
     * @return Map of String,Object including id value, excluding label value
     */
    private Map<String,Object> getPropertiesFromObject(RedisObject ro){
        Map<String, Object> properties = new HashMap<String,Object>();
        String idString = ro.getId();
        Long id = Long.parseLong(idString);
        properties = ((RedisEntity) ro).getProperties();
        if (properties.containsKey("id")) properties.remove("id");
        properties.remove("label");
        return properties;
    }

    /**
     * Helper method to get a List of Labels for a given RedisObject
     * @param ro allowed: RedisEntity
     * @return ArrayList of Labels
     */
    private Label[] getLabelsFromObject(RedisObject ro){
        Map<String, Object> properties;
        properties = ((RedisEntity) ro).getProperties();
        List<Label> labels = new ArrayList<Label>();
        if (properties.containsKey("label")){
            for (String l : ((String)properties.get("label")).split(":")) {
                labels.add(DynamicLabel.label(l));
            }
        }
        return labels.toArray(new Label[labels.size()]);
    }
}