
import com.avellinus.neo4jRedisPlugin.neo4j.NeoDaemon;
import com.avellinus.neo4jRedisPlugin.utlis.Settings;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;


/**
 * Tests for graf.rotz
 */
public class test_plugin {
    private static GraphDatabaseService db;
    private static Settings settings = Settings.getInstance();
    private static String DB_PATH = settings.neoDBPath;
    @Before
    public void prepareTestData()
    {
        db = new GraphDatabaseFactory()
                .newEmbeddedDatabaseBuilder(DB_PATH)
                .newGraphDatabase();
    }

    @After
    public void destroyTestDatabase()
    {
        db.shutdown();
    }

    @Test
    public void test_simple_import() throws Exception{
        NeoDaemon d = new NeoDaemon(db);
        d.run();
    }

    @Test
    public void test_simple_import_diagnostic() throws Exception{
        NeoDaemon d = new NeoDaemon(db);
        d.run();
    }

}
